
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var EHRIDs = [];
var toggled = false;

/**	
	GOOGLE MAPS API KEY:
	
	AIzaSyD4y7ttHSUKIGvjlRSYX081pZirjZ9iJm4
	
	GOOGLE MAPS DOCUMENTATION:
	
	https://developers.google.com/maps/documentation/javascript/reference
**/

/**
BACKUP DATA

https://creately.com/

VOPC DIAGRAM DATA
		
<<boundary>>
index.html
--
# ime : input_text
# priimek : input_text
# datumrojstva : input_text
# EHRID_input : input_text
# date_input : input_text
# height_input : input_text
# weight_input : input_text
# sys_input : input_text
# dia_input : input_text
# perc_input : input_text
# EHRID_register : input_text
--
+ registriraj()
+ generirajNakljucnePodatke()
+ dodajMeritev()
+ generirajNakljucnaMeritev()
+ prijava()
+ generiranjePodatkov()

<<control>>
koda.js
--
- baseUrl : String
- queryUrl : String
- username : String
- password : String
- EHRIDs : JSONArray
- toggled : boolean
- maxDays : Array
- map : google.maps.Map
- infoWindow : google.maps.InfoWindow
--
# getSessionId() : String
+ createNewEHR() : void
# getName(String) : String
# showDetailedData(String) : void
# showDetails(String) : void
+ addNewEntry() : void
# getShortDate(String) : String
+ displayAllData(boolean) : void
+ generirajPodatke(int) : void
# getRandomData() : void
+ fillRandomDataFirst() : void
+ fillRandomDataSecond(boolean) : void
# initMap() : void
# callback(results, status) : void
# createMarker(place) : void
# handleLocationError(boolean,
infoWindow,  position) : void

<<boundary>>
EhrScapeAPI
--
# https://rest.ehrscape.com/rest/v1

<<boundary>>
Demographics
--
# Saves a new party
+ POST : demographics/party(String header, String body)
# Gets the demographics information for the party
+ GET : demographics/ehr/{ehrId}/party(String header,
String ehrId, ...) : JSONObject party

<<boundary>>
OpenEHRCompositions
--
# Saves a new composition
+ POST : composition(String header, String ehrId, String templateId,
String format, JSONObject data, ...) : String meta.href

<<boundary>>
HealthDataViews
--
# Gets the recorded systolic and diastolic blood pressure for the patient
+ GET : view/{ehrId}/blood_pressure(String header, String ehrId) : JSONArray result
# Gets the recorded height for the patient
+ GET : view/{ehrId}/height(String header, String ehrId) : JSONArray result
# Gets the recorded SpO2 measurements for the patient
+ GET : view/{ehrId}/spO2(String header, String ehrId) : JSONArray result
# Gets the recorded weight for the patient
+ GET : view/{ehrId}/weight(String header, String ehrId) : JSONArray result

<<boundary>>
ElectronicHealthRecordManagement
-- 
# Creates a new EHR record
+ POST : ehr(String header, ...) : String ehrId

<<boundary>>
SessionManagement
--
# Logs in and creates a new session
+ POST : session(String username,
String password) : String sessionId

<<boundary>>
Google Maps API
--
+ map : google.maps.Map
+ infoWindow : google.maps.InfoWindow
--
# google.maps.Map(HTMLElement, JSONObject) : google.maps.Map
# google.maps.InfoWindow() : google.maps.InfoWindow
# Geolocation.getCurrentPosition(function(JSONObject), function()) : void
# google.maps.InfoWindow().setPosition(JSONObject) : void
# google.maps.InfoWindow().setContent(String) : void
# google.maps.InfoWindow().open(google.maps.Map) : void
# google.maps.LatLng(double, double) : google.maps.LatLng
# google.maps.places.PlacesService(google.maps.Map) : google.maps.places.PlacesService
# google.maps.places.PlacesService(google.maps.Map).nearbySearch(JSONObject, callback) : void
# google.maps.Marker(JSONObject) : google.maps.Marker
# google.maps.event.addListener(google.maps.Marker, String, function()) : void
**/

function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function createNewEHR() {
	sessionId = getSessionId();
	
	var ime = document.getElementById("ime").value; 				  /// $("#ime").val();
	var priimek = document.getElementById("priimek").value; 	      /// $("#priimek").val();
	var datumRojstva = document.getElementById("datumrojstva").value; /// $("#datumrojstva").val();
	
	if (!ime || !priimek || !datumRojstva || !ime.trim().length || !priimek.trim().length || !datumRojstva.trim().length) {
		$("#registracijaPoraka").html('										\
			<div style="text-align: center;" class="text-danger">			\
				<h5>Napaka!</h5>											\
				<h5>Izpolnite in preverite natancnost vseh polj!</h5>		\
			</div>															\
		');
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
				
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
							EHRIDs.push({EHRID: ehrId, fn: ime, ln:	priimek, bd: datumRojstva});
							$("#registracijaPoraka").html('										\
								<div style="text-align: center;" class="text-success">			\
									<h5>Vi ste bili uspešno registrirani!</h5>					\
									<h5>Vaš EHR ID je:</h5>' + ehrId + ' 						\
								</div>															\
							');
							$("#seznamUporabnikov").append('<li><div onclick="showDetails(\'' + ehrId + '\')">' + ime + ' ' + priimek + ' <b>' + ehrId + '</b></div></li>');
		                }
		            },
		            error: function(err) {
						$("#registracijaPoraka").html('										\
							<div style="text-align: center;" class="text-danger">			\
								<h5>Napaka!</h5>											\
								<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
							</div>															\
						');
		            }
		        });
		    }
		});
	}
}

function getName(EHRID) {
	for (var i = 0; i < EHRIDs.length; i++) {
		if (EHRID == EHRIDs[i].EHRID) {
			return EHRIDs[i].fn + ' ' + EHRIDs[i].ln;
		}
	}
}

function showDetailedData(EHRID) {
	sessionId = getSessionId();
	
	toggled = false;
	
	var allData = [];	/// {date: <date>, height: <height>, weight: <weight>, sysbp: <sys>, diabp: <diabp>, perc: <perc>}	
	
	/**
		<div style="text-align: center;" class="text-success">											\
			<h5>Podrobnosti o vseh meritvah</h5>														\
		</div>																							\
		<br/>																							\
	**/
	
	$("#vsehMeritvah").html('																			\
		<div style="text-align: center;">																\
			<button id="podrobnosti" class="w3-btn w3-blue-grey">Prikaži / skrij podrobnosti</button>	\
		</div>																							\
		<br/>																							\
		<ol id="seznamVsehMeritev" style="text-align: center;" class="w3-text-teal">					\
		</ol>																							\
	');
	
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
		
	$.ajax({
		url: baseUrl + "/view/" + EHRID + "/height",
		type: "GET",
		success: function(heightData) {
			for (var i = 0; i < heightData.length; i++) {
				allData.push({
					date: getShortDate(heightData[i].time),
					height: heightData[i].height + ' ' + heightData[i].unit
				});
			}
			$.ajax({
				url: baseUrl + "/view/" + EHRID + "/weight",
				type: "GET",
				success: function(weightData) {
					for (var i = 0; i < weightData.length; i++) {
						for (var j = 0; j < allData.length; j++) {
							if (allData[j].date == getShortDate(weightData[i].time)) {
								allData[j].weight = weightData[i].weight + ' ' + weightData[i].unit;
							}
						}
					}
					$.ajax({
						url: baseUrl + "/view/" + EHRID + "/blood_pressure",
						type: "GET",
						success: function(presData) {
							for (var i = 0; i < presData.length; i++) {
								for (var j = 0; j < allData.length ; j++) {
									if (allData[j].date == getShortDate(presData[i].time)) {
										allData[j].sysbp = presData[i].systolic + ' ' + presData[i].unit;
										allData[j].diabp = presData[i].diastolic + ' ' + presData[i].unit;
									}
								}
							}
							$.ajax({
								url: baseUrl + "/view/" + EHRID + "/spO2",
								type: "GET",
								success: function(percData) {
									for (var i = 0; i < percData.length; i++) {
										for (var j = 0; j < allData.length; j++) {
											if (allData[j].date == getShortDate(percData[i].time)) {
												allData[j].perc = percData[i].spO2 + '%';
											}
										}
									}
									for (var i = 0; i < allData.length; i++) {
										$("#seznamVsehMeritev").append('																			\
											<li>Datum: ' + allData[i].date + ' | Visina: ' + allData[i].height + ' | Tezina:						\
											' + allData[i].weight + ' | Diastolicni krvni tlak: ' + allData[i].diabp + ' | Sistolicni krvni tlak:	\
											' + allData[i].sysbp + ' | Nasicenost krvi s kisikom: ' + allData[i].perc + ' </div></li>					\
										');
									}
								}, error: function(err) {
									$("#vsehMeritvah").html('											\
										<div style="text-align: center;" class="text-danger">			\
											<h5>Napaka!</h5>											\
											<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
										</div>															\
									');
								}
							});
							
						}, error: function(err) {
							$("#vsehMeritvah").html('											\
								<div style="text-align: center;" class="text-danger">			\
									<h5>Napaka!</h5>											\
									<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
								</div>															\
							');
						}
					});
				}, error: function(err) {
					$("#vsehMeritvah").html('											\
						<div style="text-align: center;" class="text-danger">			\
							<h5>Napaka!</h5>											\
							<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
						</div>															\
					');
				}
			});
		}, error: function(err) {
			$("#vsehMeritvah").html('											\
				<div style="text-align: center;" class="text-danger">			\
					<h5>Napaka!</h5>											\
					<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
				</div>															\
			');
		}
	});
}

function showDetails(EHRID) {
	document.getElementById("EHRID_register").value = EHRID;
	displayAllData(true);
	showDetailedData(EHRID);
	
	/**
				<h5>Malo spodaj so prikazane podrobnosti o merithav in tudi na spodnjih grafih so prikazane podatki pri meritvah tega uporabnika</h5>			\
	**/
	
	$("#izberiPoraka").html('																																\
		<div style="text-align: center;" class="text-success">																								\
			<h5>Izbrali ste uporabnika ' + getName(EHRID) + ' z EHRID-om ' + EHRID + '</h5>																	\
		</div>																																				\
	');
}

function addNewEntry() {
	sessionId = getSessionId();

	var ehrId = document.getElementById("EHRID_input").value;							/// $("#dodajVitalnoEHR").val();
	var datumInUra = document.getElementById("date_input").value;						///	$("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = document.getElementById("height_input").value;					/// $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = document.getElementById("weight_input").value;					/// $("#dodajVitalnoTelesnaTeza").val();
	var sistolicniKrvniTlak = document.getElementById("sys_input").value;				/// $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = document.getElementById("dia_input").value;				/// $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = document.getElementById("perc_input").value;			/// $("#dodajVitalnoNasicenostKrviSKisikom").val();

	if (!ehrId || !datumInUra || !telesnaVisina || !telesnaTeza || !sistolicniKrvniTlak || !diastolicniKrvniTlak || !nasicenostKrviSKisikom ||
		!ehrId.trim().length || !datumInUra.trim().length || !telesnaVisina.trim().length || !telesnaTeza.trim().length || !sistolicniKrvniTlak.trim().length ||
		!diastolicniKrvniTlak.trim().length || !nasicenostKrviSKisikom.trim().length) {
		$("#meritevPoraka").html('											\
			<div style="text-align: center;" class="text-danger">			\
				<h5>Napaka!</h5>											\
				<h5>Izpolnite in preverite natancnost vseh polj!</h5>		\
			</div>															\
		');
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
				$("#meritevPoraka").html('											\
					<div style="text-align: center;" class="text-success">			\
						<h5>Meritev je bila uspesno dodana v seznamu!</h5>			\
					</div>															\
				');
		    },
		    error: function(err) {
				$("#meritevPoraka").html('											\
					<div style="text-align: center;" class="text-danger">			\
						<h5>Napaka!</h5>											\
						<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
					</div>															\
				');
		    }
		});
	}
}

function getShortDate(date) {
	return (date.indexOf("T") > -1 ? date.substring(0, date.indexOf("T")) : date);
}

function displayAllData(shown) {
	sessionId = getSessionId();
	
	var ehrId = document.getElementById("EHRID_register").value;
	
	if (!ehrId || !ehrId.trim().length) {
		$("#seznamMeritev").html('											\
			<div style="text-align: center;" class="text-danger">			\
				<h5>Napaka!</h5>											\
				<h5>Izpolnite polje za EHR ID!</h5>							\
			</div>															\
		');
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: "GET",
			success: function(data) {
				if (!shown) {
					showDetails(ehrId);
				} else {
					var party = data.party;
					$("#prijavaPoraka").html('																		\
						<div style="text-align: center;" class="text-success">										\
							<h5>Uspešno ste se prijavili kot ' + party.firstNames + ' ' + party.lastNames + '!</h5>	\
						</div>																						\
					');

					$.ajaxSetup({
						headers: {"Ehr-Session": sessionId}
					});
					
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/height",
						type: "GET",
						success: function(heightData) {
							$("#seznamMeritev").html('															\
								<div style="text-align: center;" class="text-success">							\
									<h5>Na voljo imate podatke za ' + heightData.length + ' mertivi</h5>		\
								</div>																			\
							');
						},
						error: function(err) {
							$("#meritevPoraka").html('											\
								<div style="text-align: center;" class="text-danger">			\
									<h5>Napaka!</h5>											\
									<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
								</div>															\
							');
						}
					});
					
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/spO2",
						type: "GET",
						success: function(percData) {
							var pts = [];
							for (var i = 0; i < percData.length; i++) {
								pts.push({
									label: getShortDate(percData[i].time),
									y: percData[i].spO2
								});
							}
							for (var i = 0; i < pts.length / 2; i++) {
								var temp = pts[i];
								pts[i] = pts[pts.length-1-i];
								pts[pts.length-1-i] = temp;
							}
							var chart = new CanvasJS.Chart("percChart", {
								data: [              
									{
										type: "line",
										dataPoints: pts
									}
								]
							});
							chart.render();
						},
						error: function(err) {
							$("#percChart").html('												\
								<div style="text-align: center;" class="text-danger">			\
									<h5>Napaka!</h5>											\
									<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
								</div>															\
							');
						}
					});
					
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/height",
						type: "GET",
						success: function(heightData) {
							var pts = [];
							for (var i = 0; i < heightData.length; i++) {
								pts.push({
									label: getShortDate(heightData[i].time),
									y: heightData[i].height
								});
							}
							for (var i = 0; i < pts.length / 2; i++) {
								var temp = pts[i];
								pts[i] = pts[pts.length-1-i];
								pts[pts.length-1-i] = temp;
							}
							var chart = new CanvasJS.Chart("heightChart", {
								data: [              
									{
										type: "line",
										dataPoints: pts
									}
								]
							});
							chart.render();
						},
						error: function(err) {
							$("#heightChart").html('											\
								<div style="text-align: center;" class="text-danger">			\
									<h5>Napaka!</h5>											\
									<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
								</div>															\
							');
						}
					});
					
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/weight",
						type: "GET",
						success: function(weightData) {
							var pts = [];
							for (var i = 0; i < weightData.length; i++) {
								pts.push({
									label: getShortDate(weightData[i].time),
									y: weightData[i].weight
								});
							}
							for (var i = 0; i < pts.length / 2; i++) {
								var temp = pts[i];
								pts[i] = pts[pts.length-1-i];
								pts[pts.length-1-i] = temp;
							}
							var chart = new CanvasJS.Chart("wieghtChart", {
								data: [              
									{
										type: "line",
										dataPoints: pts
									}
								]
							});
							chart.render();
						},
						error: function(err) {
							$("#wieghtChart").html('											\
								<div style="text-align: center;" class="text-danger">			\
									<h5>Napaka!</h5>											\
									<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
								</div>															\
							');
						}
					});
					
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/blood_pressure",
						type: "GET",
						success: function(bloodPressureData) {
							var sysPts = [], diaPts = [];
							for (var i = 0; i < bloodPressureData.length; i++) {
								sysPts.push({
									label: getShortDate(bloodPressureData[i].time),
									y: bloodPressureData[i].systolic
								});
								diaPts.push({
									label: getShortDate(bloodPressureData[i].time),
									y: bloodPressureData[i].diastolic
								});
							}
							for (var i = 0; i < sysPts.length / 2; i++) {
								var temp = sysPts[i];
								sysPts[i] = sysPts[sysPts.length-1-i];
								sysPts[sysPts.length-1-i] = temp;
							}
							for (var i = 0; i < diaPts.length / 2; i++) {
								var temp = diaPts[i];
								diaPts[i] = diaPts[diaPts.length-1-i];
								diaPts[diaPts.length-1-i] = temp;
							}
							var chartSys = new CanvasJS.Chart("sysbpChart", {
								data: [              
									{
										type: "line",
										dataPoints: sysPts
									}
								]
							});
							var chartDia = new CanvasJS.Chart("diabpChart", {
								data: [              
									{
										type: "line",
										dataPoints: diaPts
									}
								]
							});
							chartSys.render();
							chartDia.render();
						},
						error: function(err) {
							$("#sysbpChart").html('												\
								<div style="text-align: center;" class="text-danger">			\
									<h5>Napaka!</h5>											\
									<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
								</div>															\
							');
							$("#diabpChart").html('												\
								<div style="text-align: center;" class="text-danger">			\
									<h5>Napaka!</h5>											\
									<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
								</div>															\
							');
						}
					});
				}
			}, error: function(err) {
				$("#prijavaPoraka").html('											\
					<div style="text-align: center;" class="text-danger">			\
						<h5>Napaka!</h5>											\
						<h5>' + JSON.parse(err.responseText).userMessage + '</h5>	\
					</div>															\
				');
			}
		});
	}
}

function generirajPodatke(stPacienta) {
	fillRandomDataFirst();
	setTimeout(function() {
		createNewEHR();
		setTimeout(function() {
			fillRandomDataSecond(true);
			setTimeout(function() {
				addNewEntry();
				setTimeout(function() {
					fillRandomDataSecond(true);
					setTimeout(function() {
						addNewEntry();
						setTimeout(function() {
							fillRandomDataSecond(true);
							setTimeout(function() {
								addNewEntry();
								setTimeout(function() {
									fillRandomDataSecond(true);
									setTimeout(function() {
										addNewEntry();
									}, 500);
								}, 500);
							}, 500);
						}, 500);
					}, 500);
				}, 500);					
			}, 500);
		}, 500);
	}, 500);
}


function getRandomData() {
	var fn = ["Mile", "Toso", "Trajko", "Zoran", "David"];
	var ln = ["Panika", "Malerot", "Risteski", "Jordanoski", "Petkoski"];
	var random = [];
	for (var i = 0; i < fn.length; i++) {
		for (var j = 0; j < ln.length; j++) {
			random.push({fn: fn[i], ln: ln[j], bd: generateRandomDate()});
		}
	}
	/**
	random.push({fn: "Mile",   ln: "Panika",   bd: "1969-11-12T07:17"});
	random.push({fn: "Toso",   ln: "Malerot",  bd: "1987-12-09T17:06"});
	random.push({fn: "Trajko", ln: "Risteski", bd: "2008-01-05T22:51"});
	**/
	return random;
}

var maxDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

function generateRandomDate() {
	var year = 1950 + Math.round(67 * Math.random());
	var month = 1 + Math.round(11 * Math.random());
	var day = Math.min(maxDays[month-1], Math.max(1, Math.round(31 * Math.random())));
	var hour = Math.round(23 * Math.random());
	var minute = Math.round(59 * Math.random());
	return year + "-" +
		  (month  < 10 ? "0" + month : month )  + "-" +
		  (day    < 10 ? "0" + day   : day   )  + "T" +
		  (hour   < 10 ? "0" + hour  : hour  )  + ":" +
		  (minute < 10 ? "0" + minute: minute)  + "Z";
}

function fillRandomDataFirst() {
	var idx = Math.floor(25 * Math.random());
	var podatki = getRandomData()[idx];
	document.getElementById("ime").value = podatki.fn;
	document.getElementById("priimek").value = podatki.ln;
	document.getElementById("datumrojstva").value = podatki.bd;
}

function fillRandomDataSecond(last) {
	document.getElementById("EHRID_input").value =  EHRIDs[last ? EHRIDs.length - 1 : Math.floor(EHRIDs.length * Math.random())].EHRID; /// Math.min(EHRIDs.length - 1, Math.round(EHRIDs.length * Math.random()))
	document.getElementById("date_input").value =   generateRandomDate();
	document.getElementById("height_input").value = 130 + Math.round( 80 * Math.random());
	document.getElementById("weight_input").value = 45  + Math.round(115 * Math.random());
	document.getElementById("sys_input").value =    100 + Math.round(100 * Math.random());
	document.getElementById("dia_input").value =    55  + Math.round( 50 * Math.random());
	document.getElementById("perc_input").value =   85  + Math.round( 14 * Math.random());
}

$(document).ready(function() {
	$("#randomPodatke").click(function() {
		fillRandomDataFirst();
	});
	
	$("#randomPodatke_input").click(function() {
		if (!EHRIDs.length) {
			$("#meritevPoraka").html('															\
				<div style="text-align: center;" class="text-danger">							\
					<h5>Napaka!</h5>															\
					<h5>V trenutni seji se še nimate registrirano!								\
					Zato se random podatke ne morejo generirati!</h5>							\
				</div>																			\
			');
		} else {
			$("#meritevPoraka").html('');
			fillRandomDataSecond(false);
		}
	});
	
	$("#register_first").click(function() {
		createNewEHR();
	});
	
	$("#dodajMeritev").click(function() {
		addNewEntry();
	});
	
	$("#generiraj").click(function() {
		generirajPodatke(1);
		setTimeout(function() {
			generirajPodatke(2);
			setTimeout(function() {
				generirajPodatke(3);
			}, 6000);
		}, 6000);
	});
	
	$("#login").click(function() {
		displayAllData(false);
	});
	
	$(document).on('click', '#podrobnosti', function() {
		if (toggled) {
			$("#seznamVsehMeritev").show();
		} else {
			$("#seznamVsehMeritev").hide();
		}
		toggled ^= true; /// toggled = !toggled;
	});
	
	initMap();
});


/** GOOGLE MAPS API CODE HERE **/


var map, infoWindow;

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
	  center: {lat: 46.0569, lng: 14.5058},
	  zoom: 10
	});
	infoWindow = new google.maps.InfoWindow();

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};

			infoWindow.setPosition(pos);
			infoWindow.setContent('Location found.');
			infoWindow.open(map);
			map.setCenter(pos);
			
			var pos2 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			var service = new google.maps.places.PlacesService(map);
			service.nearbySearch({
				location: pos2,
				radius: 50000,
				type: ['hospital']
			}, callback);
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		});
	} else {
	  handleLocationError(false, infoWindow, map.getCenter());
	}
}

function callback(results, status) {
	if (status === google.maps.places.PlacesServiceStatus.OK) {
		for (var i = 0; i < results.length; i++) {
			createMarker(results[i]);
		}
	}
}

function createMarker(place) {
	var placeLoc = place.geometry.location;
	var marker = new google.maps.Marker({
		map: map,
		position: place.geometry.location
	});

	google.maps.event.addListener(marker, 'click', function() {
		infoWindow.setContent(place.name);
		infoWindow.open(map, this);
	});
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow.setContent(browserHasGeolocation ?
						  'Error: The Geolocation service failed.' :
						  'Error: Your browser doesn\'t support geolocation.');
	infoWindow.open(map);
}